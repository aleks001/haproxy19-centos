#!/bin/bash

set -e

export TZ=${TZ:-UTC}
export LOGLEVEL=${LOGLEVEL:-notice}

if [[ -n "${DEBUG}" ]]; then

 set -x

echo "Current ENV Values"
echo "==================="
echo "SERVICE_NAME        :"${SERVICE_NAME}
echo "SERVICE_DEST        :"${SERVICE_DEST}
echo "SERVICE_DEST_PORT   :"${SERVICE_DEST_PORT}
echo "TZ                  :"${TZ}
echo "CONFIG_FILE         :"${CONFIG_FILE}
echo "given DNS_SRV001    :"${DNS_SRV001}
echo "given DNS_SRV002    :"${DNS_SRV002}

echo "HAProxy Version:"

/usr/local/sbin/haproxy -vv

echo "params" $@
echo "----  -----"
echo "param1" $1
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- haproxy "$@"
fi

if [ "$1" = 'haproxy' ]; then
	shift # "haproxy"
	# if the user wants "haproxy", let's add a couple useful flags
	#   -W  -- "master-worker mode" (similar to the old "haproxy-systemd-wrapper"; allows for reload via "SIGUSR2")
	#   -db -- disables background mode
	set -- haproxy -W -S /var/run/hap-master-socket -db "$@"
fi

exec "$@"