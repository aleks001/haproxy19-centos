FROM centos:latest

# take a look at http://www.lua.org/download.html for
# newer version

ENV HAPROXY_MAJOR=1.9 \
    HAPROXY_VERSION=1.9.8 \
    HAPROXY_SHA256=2d9a3300dbd871bc35b743a83caaf50fecfbf06290610231ca2d334fd04c2aee \
    LUA_VERSION=5.3.5 \
    LUA_URL=http://www.lua.org/ftp/lua-5.3.5.tar.gz \
    LUA_SHA256=112eb10ff04d1b4c9898e121d6bdf54a81482447 \
    HAPSCRAP_VERS=0.10.0 \
    OPENSSL_VERS=1.1.1b \
    OSSP_SHA256=5c557b023230413dfb0756f3137a13e6d726838ccd1430888ad15bfb2b43ea4b

RUN set -x \
  && yum -y update \
  && export buildDeps='which python3 git pcre-devel openssl-devel gcc make zlib-devel readline-devel openssl perl-Module-Load-Conditional perl-Test-Harness ' \
  && yum -y install pcre zlib bind-utils curl iproute tar strace python3 ${buildDeps} \
  && mkdir -p /usr/src/openssl /usr/src/lua /usr/src/haproxy \
  && curl -sSLO https://www.openssl.org/source/openssl-${OPENSSL_VERS}.tar.gz \
  && echo "${OSSP_SHA256} openssl-${OPENSSL_VERS}.tar.gz" | sha256sum -c \
  && tar xfvz openssl-${OPENSSL_VERS}.tar.gz -C /usr/src/openssl --strip-components=1 \
  && cd /usr/src/openssl \
  && ./config --prefix=/usr/local/openssl --openssldir=/usr/local/openssl shared zlib \
  && make \
  && make install \
  && echo "pathmunge /usr/local/openssl/bin" > /etc/profile.d/openssl.sh \
  && echo "/usr/local/openssl/lib" > /etc/ld.so.conf.d/openssl-${OPENSSL_VERS}.conf \
  && ldconfig -v \
  && cd /usr/src/ \
  && git clone https://github.com/vtest/VTest.git \
  && cd VTest \
  && make vtest \
  && cd /usr/src/ \
  && curl -sSLO ${LUA_URL} \
  && echo "${LUA_SHA256} lua-${LUA_VERSION}.tar.gz" | sha1sum -c - \
  && tar -xzf lua-${LUA_VERSION}.tar.gz -C /usr/src/lua --strip-components=1 \
  && make -C /usr/src/lua linux test install \
  && curl -sSLO http://www.haproxy.org/download/${HAPROXY_MAJOR}/src/haproxy-${HAPROXY_VERSION}.tar.gz \
  && echo "${HAPROXY_SHA256} haproxy-${HAPROXY_VERSION}.tar.gz" | sha256sum -c \
  && tar -xzf haproxy-${HAPROXY_VERSION}.tar.gz  -C /usr/src/haproxy --strip-components=1 \
  && make -C /usr/src/haproxy  \
       TARGET=linux2628 \
       USE_PCRE=1 \
       USE_OPENSSL=1 \
       SSL_INC=/usr/local/openssl/include \
       SSL_LIB=/usr/local/openssl/lib \
       USE_ZLIB=1 \
       USE_LINUX_SPLICE=1 \
       USE_TFO=1 \
       USE_PCRE_JIT=1 \
       USE_LUA=1 \
       USE_PTHREAD_PSHARED=1 \
       USE_REGPARM=1 \
       USE_GETADDRINFO=1 \
       USE_THREAD=1 \
       EXTRA_OBJS="contrib/prometheus-exporter/service-prometheus.o" \
       all \
       install-bin \
  && mkdir -p /usr/local/etc/haproxy \
  && mkdir -p /usr/local/etc/haproxy/ssl \
  && mkdir -p /usr/local/etc/haproxy/ssl/cas \
  && mkdir -p /usr/local/etc/haproxy/ssl/crts \
  && cp -R /usr/src/haproxy/examples/errorfiles /usr/local/etc/haproxy/errors \
  && curl -SL https://github.com/prometheus/haproxy_exporter/releases/download/v${HAPSCRAP_VERS}/haproxy_exporter-${HAPSCRAP_VERS}.linux-amd64.tar.gz \
     | tar xzvf - \
  && mv haproxy_exporter-${HAPSCRAP_VERS}.linux-amd64/haproxy_exporter /usr/local/sbin/ \
  && cd /usr/src/haproxy \
  && VTEST_PROGRAM=/usr/src/VTest/vtest HAPROXY_PROGRAM=/usr/local/sbin/haproxy \
      make reg-tests -- --use-htx \
  ; egrep -r ^ /tmp/haregtests*/* \
  ; yum -y autoremove $buildDeps \
  && yum -y clean all \
  && rm -rf /usr/src/ \
  && /usr/local/sbin/haproxy -vv


#         && openssl dhparam -out /usr/local/etc/haproxy/ssl/dh-param_4096 4096 \

COPY containerfiles /

RUN chmod 555 /container-entrypoint.sh

EXPOSE 13443

ENTRYPOINT ["/container-entrypoint.sh"]

#CMD ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.conf"]
#CMD ["haproxy", "-vv"]
